'use strict';
import React, {
  AppRegistry,
  Component,
  NavigatorIOS,
  StyleSheet,
} from 'react-native';
import {Main} from './src/components/ios/main';

class BrNativeWebExam extends Component {
  render() {
    return (
      <NavigatorIOS
        barTintColor="#43e895"
        titleTextColor="#fff"
        style={styles.container}
        initialRoute={{
          title: "Lunch Tyme",
          component: Main
        }} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

AppRegistry.registerComponent('BrNativeWebExam', () => BrNativeWebExam);
