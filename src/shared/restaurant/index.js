import {Restaurant} from './restaurant.model';
import {RestaurantService} from './restaurant.service';
export {
  Restaurant,
  RestaurantService
};
