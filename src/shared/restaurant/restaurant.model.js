export class Restaurant {
  constructor(obj) {
    obj = (obj) ? obj : {};
    this.name = obj.name || null;
    this.backgroundImageURL = obj.backgroundImageURL || null;
    this.category = obj.category || null;
    this.contact = obj.contact || {};
    this.location = new Location(obj.location);
  }
}

class Location {
  constructor(obj) {
    obj = (obj) ? obj : {};
    this.address = obj.address || null;
    this.crossStreet = obj.crossStreet || null;
    this.lat = obj.lat || null;
    this.lng = obj.lng || null;
    this.postalCode = obj.postalCode || null;
    this.cc = obj.cc || null;
    this.city = obj.city || null;
    this.country = obj.country || null;
    this.formattedAddress = obj.formattedAddress || [];
  }
}
