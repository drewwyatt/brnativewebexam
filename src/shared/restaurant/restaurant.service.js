'use strict';
import {Restaurant} from './restaurant.model';
export class RestaurantService {
  static getFeed() {
    const url = 'http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json';
    return fetch(url).then(response => response.json()).then(response => response.restaurants.map(s => new Restaurant(s)));
  }
}
