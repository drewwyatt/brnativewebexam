'use strict';
import React, {
  ActivityIndicatorIOS,
  Component,
  ListView,
  Text,
  View
} from 'react-native';
import {
  Restaurant,
  RestaurantService} from '../../../shared/restaurant';
import {styles} from './main.ios.styles';
import {RestaurantCell} from './restaurant.ios';

export class Main extends Component {
  constructor() {
    super();
    this._initialize();
  }
  componentDidMount() {
    RestaurantService.getFeed()
      .then(r => this._handleResponse(r));
  }
  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <RestaurantCell data={rowData} />}
        />
      </View>
    );
  }

  // private
  _initialize() {
    this.__ds = null;

    this.state = {
      isLoading: true,
      dataSource: this._ds.cloneWithRows([]),
    };
  }

  get _ds() {
    if(!this.__ds) {
      this.__ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    }

    return this.__ds;
  }

  _handleResponse(r) {
    this.setState({
      isLoading: false,
      dataSource: this._ds.cloneWithRows(r),
    });
  }
}
