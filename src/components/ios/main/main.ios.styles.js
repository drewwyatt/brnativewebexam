'use strict';
import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    flex: 1,
    height: 180,
  },
  mainContainer: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#48BBEC'
  },
  restaurantText: {
    backgroundColor: "transparent",
    bottom: 6,
    left: 12,
    position: 'absolute',
    textAlign: 'center',
    color: '#fff'
  },
  restaurantName: {
    bottom: 22,
    fontFamily: "AvenirNext-DemiBold",
    fontSize: 22,
  },
  restaurantCategory: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 16,
  }
});
