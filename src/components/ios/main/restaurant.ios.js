'use strict';
import React, {
  Component,
  Image,
  Text,
  View
} from 'react-native';
import {styles} from './main.ios.styles.js';

export class RestaurantCell extends Component {
  componentDidMount() {
    // console.log(this.props);
  }

  render() {
    return (
        <Image source={{uri: this.props.data.backgroundImageURL}} style={styles.image}>
            <Text style={[styles.restaurantText, styles.restaurantName]}>{this.props.data.name}</Text>
            <Text style={[styles.restaurantText, styles.restaurantCategory]}>{this.props.data.category}</Text>
        </Image>
    );
  }
}
